# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2021"
__credits__ = ["Morten Lind"]
__license__ = "AGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import math3d as m3d

from pymoco.kinematics import joints
from pymoco.robots.robot_definition import RobotDefinition


class KUKAKR60_L30_3(RobotDefinition):

    def __init__(self, **kwargs):
        RobotDefinition.__init__(self, **kwargs)

        self._dof = 6

        # Joint position limits
        self._pos_lim_act = self._pos_lim_ser = np.deg2rad(np.array(
            [[-185, 185],
             [-135, 35],
             [-120, 158],
             [-350, 350],
             [-119, 119],
             [-350, 350]],
            dtype=np.double)).T

        # Joint speed limits
        self._spd_lim_act = self._spd_lim_ser = np.deg2rad(np.array(
            [128, 101, 128, 260, 245, 322],
            dtype=np.double)).T

        # Home pose
        self._q_home = np.array([0.0, -np.pi/2, np.pi/2, 0.0, np.pi/2, 0.0],
                                dtype=np.float64)
        
        self._link_xforms = [m3d.Transform() for i in range(self._dof + 1)]

        self._link_xforms[0].orient.rotate_xb(np.pi)

        self._link_xforms[1].orient.rotate_xb(np.pi/2)
        self._link_xforms[1].pos.x = 0.350
        self._link_xforms[1].pos.z = -0.815

        self._link_xforms[2].pos.x = 0.850

        self._link_xforms[3].orient.rotate_yb(-np.pi/2)
        self._link_xforms[3].pos.y = -0.145
        self._link_xforms[3].pos.x = 1.220

        self._link_xforms[4].orient.rotate_yb(np.pi/2)

        self._link_xforms[5].orient.rotate_yb(-np.pi/2)
        self._link_xforms[5].pos.x = 0.170

        self._link_xforms[6].orient.rotate_yb(np.pi)
        self._link_xforms[6].orient.rotate_zt(np.pi/2)

        self._joint_xforms = self.joint_xforms

    def get_joint_xforms(self):
        return [joints.RevoluteJoint() for i in range(self._dof)]

    joint_xforms = property(get_joint_xforms)
