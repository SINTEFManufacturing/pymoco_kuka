# coding=utf-8

"""
A facade class for interfacing through the 'rsi_server' found in the project git@github.com:tingelst/autokons.git
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "AGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading
import socket
import struct
import time

import numpy as np

from .. import robots
from . import RobotFacade
from .. import kinematics


class KUKARSIAdapterFacade(RobotFacade, threading.Thread):

    class Error(Exception):
        pass

    def __init__(self, **kwargs):
        self._log_level = kwargs.get('log_level', 2)
        RobotFacade.__init__(self, **kwargs)
        self._rob_def = robots.get_robot_type(kwargs['rob_type'])()
        self._frame_computer = kinematics.FrameComputer(
            rob_def=self._rob_def)
        self._kra_host = kwargs.get('rob_host', '127.0.0.1')
        self._kra_port = kwargs.get('rob_port', 49500)
        self._kra_addr = (self._kra_host, self._kra_port)
        threading.Thread.__init__(self, name=repr(self))
        self.daemon = True
        self.__stop = False
        self._kra_sock = socket.socket(type=socket.SOCK_DGRAM)
        self._kra_sock.bind(self._kra_addr)
        self._packet_counter = 0
        self._cycle_time = kwargs.get('cycle_time', 0.01)

    def __repr__(self):
        return '{}<{}:{}>'.format(self.__class__.__name__,
                                  self._kra_host,
                                  self._kra_port)

    def get_current_arrival_time(self):
        """Return the time at which the current, i.e. latest, status
        packet was received from the robot controller."""
        return self._t_packet

    current_arrival_time = property(get_current_arrival_time)

    def set_cmd_joint_pos(self, cmd_jpos):
        self._kra_sock.sendto(struct.pack('Q6d', self._ipoc, *cmd_jpos),
                              self._kra_addr)
        self._q_inc = cmd_jpos - self._q_tracked
        self._q_tracked = cmd_jpos
        # Keep the frame computer updated
        self._frame_computer.joint_angles_vec = self._q_tracked
        with self._control_cond:
            self._control_cond.notify_all()

    cmd_joint_pos = property(RobotFacade.get_cmd_joint_pos, set_cmd_joint_pos)

    def stop(self, join=False):
        # Send a quit packet
        self.__stop = True
        if self.join:
            self.join()

    def _recv_act(self):
        bytes_, self._kra_addr = self._kra_sock.recvfrom(1024)
        data = struct.unpack('Q6d', bytes_)
        return data[0], np.array(data[1:])

    def run(self):
        # Initialize
        self._ipoc, self._q_actual = self._recv_act()
        print('Initialized, q_actual={}'.format(self._q_actual))
        self._q_tracked = self._q_actual.copy()
        self._q_error = self._q_tracked - self._q_actual
        self._q_increment = np.zeros(self._rob_def.dof)
        self._frame_computer.joint_angles_vec = self._q_tracked
        self._t_packet = time.time()
        while not self.__stop:
            self._ipoc, self._q_actual = self._recv_act()
            self._t_packet_last = self._t_packet
            self._t_packet = time.time()
            self._q_error = self._q_tracked - self._q_actual
            if np.linalg.norm(self._q_error) > self._tracking_tol:
                self.cmd_joint_pos = self._q_actual
                raise self.Error(
                    f'Tracking error ({np.linalg.norm(self._q_error)}) ' +
                    f'exceeding tolerance ({self._tracking_tol})!')

            self._event_publisher.publish(self._t_packet)
            self._cycle_time = (0.1 * self._cycle_time +
                                0.9 * (self._t_packet - self._t_packet_last))
