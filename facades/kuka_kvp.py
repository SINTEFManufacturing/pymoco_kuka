# coding=utf-8

"""
The robot controller must be set up with the KUKAVARPROXY which is accessible here: https://github.com/ImtsSrl/KUKAVARPROXY
An KRL application must be running to execute the "kvp_ax_pose" values sent via the proxy.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "AGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time
import socket
import threading
import logging
import re

import numpy as np
import math3d as m3d
from py_openshowvar import openshowvar

from .. import robots
from . import RobotFacade
from .. import kinematics


float_re = r'[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?'  # r'[+-]?\d+\.?\d*'
tool_pose_re = re.compile(', '.join([rf'{p} ({float_re})' for p in 'XYZABC']))
ax_pos_re = re.compile(
    r', '.join(6 * [rf'A{{:d}} ({float_re})'])
    .format(*range(1, 7)))
ax_pos_tmpl = '{{AXIS: ' + ', '.join([f'A{i}' + ' {:f}'
                                      for i in range(1, 7)]) + '}}'
ax_spd_tmpl = '{{KVP_AX_STRUCT: ' + ', '.join([f'V{i}' + ' {:d}'
                                               for i in range(1, 7)]) + '}}'
# ax_spd_tmpl = '[' + ', '.join(6*['{:d}']) + ']'


class KUKAKVPFacade(RobotFacade, threading.Thread):

    class Error(Exception):
        pass

    def __init__(self, **kwargs):
        kwargs['cycle_time'] = 0.1  # Fix a low cycle time
        RobotFacade.__init__(self, **kwargs)
        threading.Thread.__init__(self)
        self.daemon = True
        self._rob_def = robots.get_robot_type(kwargs['rob_type'])()
        self._frame_computer = kinematics.FrameComputer(
            rob_def=self._rob_def)
        self._kvp_host = kwargs.get('rob_host', '127.0.0.1')
        self._kvp_port = kwargs.get('rob_port',  7000)
        self._kvp_addr = (self._kvp_host, self._kvp_port)
        self._osv_lock = threading.Lock()
        self._connect()
        if not self._osv.can_connect:
            raise self.__class__.Error('__init__: Could not connect to ' +
                                       f'{self._kvp_host}:{self._kvp_port}')
        self.__stop = False

    def _read_act_joint_pos(self, attempts=5):
        attempt = 0
        with self._osv_lock:
            while attempt < attempts:
                try:
                    osv_axis_act = self._osv.read('$AXIS_ACT', debug=False)
                except AttributeError:
                    self._log.warn('AttributeError in getting actual ' +
                                   f'joint positions (attempt {attempt})')
                    attempt += 1
                else:
                    self._log.debug(osv_axis_act)
                    return np.deg2rad(
                        [float(a)
                         for a in ax_pos_re.search(
                                 osv_axis_act.decode()).groups()])
        # If attempt overflowed
        raise self.__class__.Error(
            f'Could not get actual joint position in {attempts} attempts')

    def set_cmd_joint_pos(self, cmd_jpos):
        self._q_increment = cmd_jpos - self._q_tracked
        self._q_tracked = cmd_jpos.copy()
        # kvp_spd_pct = 6 * [100]
        kvp_spd_pct = np.ceil(100 * (np.abs(self._q_increment) /
                                     self._cycle_time) /
                              self._rob_def.spd_lim_act).astype(np.uint8)
        kvp_ax_pose = np.rad2deg(self._q_tracked)
        with self._osv_lock:
            self._osv.write('KVP_AX_POSE', ax_pos_tmpl.format(*kvp_ax_pose),
                            debug=False)
            self._osv.write('KVP_AX_SPD', ax_spd_tmpl.format(*kvp_spd_pct),
                            debug=False)
            # self._osv.write('kvp_flag', 'true')
        # Keep the frame computer updated
        self._frame_computer.joint_angles_vec = self._q_tracked
        with self._control_cond:
            self._control_cond.notify_all()

    cmd_joint_pos = property(RobotFacade.get_cmd_joint_pos, set_cmd_joint_pos)

    def get_kuka_flange_pose(self):
        """Get the (measured) flange pose as reported by the KRC. The KRC
        actually reports the tool pose, so the validity of this method
        depends on the $nulltool to be configured in the controller.
        """
        with self._osv_lock:
            ktp = self._osv.read('$POS_ACT', debug=False)
        ktp_vals = [float(p)
                    for p in tool_pose_re.search(ktp.decode()).groups()]
        return m3d.Transform(
            m3d.Orientation.new_euler(np.deg2rad(ktp_vals[3:]), 'ZYX'),
            0.001 * m3d.Vector(ktp_vals[:3]))

    kuka_flange_pose = property(get_kuka_flange_pose)

    def _connect(self):
        self._log.info('Connecting')
        with self._osv_lock:
            self._osv = openshowvar(self._kvp_host, self._kvp_port)
            self._osv.sock.setsockopt(socket.IPPROTO_TCP,
                                      socket.TCP_NODELAY, 1)

    def _disconnect(self):
        self._log.info('Disconnecting')
        with self._osv_lock:
            self._osv.close()

    def __del__(self):
        self.stop()

    def stop(self, join=False):
        self.__stop = True
        if join:
            self.join()
        self._disconnect()

    def run(self):
        self._q_increment = np.zeros(self._rob_def.dof)
        self._q_actual = self._read_act_joint_pos()
        self._q_tracked = self._q_actual.copy()
        if self._q_tracked is None:
            raise self.__class__.Error(
                '.__init__: Failed to read actual joint positions!')
        # Initialize the frame computer
        self._frame_computer.joint_angles_vec = self._q_tracked
        while not self.__stop:
            t_start = time.time()
            self._q_actual = self._read_act_joint_pos()
            self._t_packet = time.time()
            self._q_error = self._q_tracked - self._q_actual
            if np.linalg.norm(self._q_error) > self._tracking_tol:
                self.cmd_joint_pos = self._q_actual
                self._disconnect()
                raise self.Error(
                    f'Tracking error ({np.linalg.norm(self._q_error)}) ' +
                    f'exceeding tolerance ({self._tracking_tol})!')
            self._event_publisher.publish(t_start)
            t_rem = self._cycle_time_nom - (time.time() - t_start)
            if t_rem > 0:
                time.sleep(t_rem)
