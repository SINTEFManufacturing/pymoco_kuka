# coding=utf-8

"""
"""

__author__ = "Lars Tingelstad"
__copyright__ = "SINTEF and NTNU 2019"
__credits__ = ["Lars Tingelstad", "Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import socket
import struct
import time


class RsiClient(object):
    def __init__(self, host='127.0.0.1', port=49500):
        # self._addr = (host, port)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind((host, port))
        
    def send(self, data):
        self._socket.sendto(data, self._addr)

    def recv(self):
        data, self._addr = self._socket.recvfrom(1024)
        return data

    def run(self):
        init_pos = np.array(struct.unpack('6d', self.recv()))
        #cmd_array = np.deg2rad([0.0, -100, 100, 0.0, 45, 0.0])
        cmd_array = init_pos.copy()
        print(init_pos)
        start_time = time.time()
        while True:
            now = time.time() - start_time
            cmd_array[4] = init_pos[4] + 0.1 * (1.0 - np.cos(now * 2.0 *
                                         np.pi * 0.1))
            cmd_struct = struct.pack('6d', *cmd_array)
            self.send(cmd_struct)

            data = self.recv()
            actual_joint_position = struct.unpack('6d', data)
            print(np.rad2deg(actual_joint_position))
            #time.sleep(0.5)


if __name__ == '__main__':
    rsi_client = RsiClient()
    rsi_client.run()
