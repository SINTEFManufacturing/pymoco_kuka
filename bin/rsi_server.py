# coding=utf-8

"""
"""

__author__ = "Lars Tingelstad"
__copyright__ = "SINTEF and NTNU 2019"
__credits__ = ["Lars Tingelstad", "Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import sys
import time
import xml.etree.ElementTree as ET
import socket
import struct
from threading import Thread, Lock

import numpy as np


# Ensure thread switching with high time resolution. (System default
# may be as high as 5e-3)
sys.setswitchinterval(1e-5)

np.set_printoptions(precision=5, suppress=True, floatmode='fixed')


class UdpServer(object):
    def __init__(self, host='127.0.0.1', port=49152):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.bind((host, port))
        self._client_addr = None

    def recv(self):
        data, addr = self._socket.recvfrom(1024)
        if self._client_addr is None:
            self._client_addr = addr
        return data

    def send(self, data):
        self._socket.sendto(data, self._client_addr)


class RsiServer(object):
    def __init__(self, rsi_host='192.168.1.87', rsi_port=49152, rist=False):
        self._rist = rist
        self._joint_zero_corr_deg = np.zeros(6, dtype=np.float64)
        # np.array([-0.00420, -90.00820, 89.99110, -0.04110, 90.05820, 0.10030]) - np.array([0,-90,90,0,90,0])
        # np.array([0.0, -0.01, -0.01, -0.04, 0.06, 0.10])
        # Server that the robot controller connects to
        self._rsi_server = UdpServer(host=rsi_host, port=rsi_port)
        # Server that an external control client connects to. Note that
        self._cmd_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UdpServer(port=49500)
        self._cmd_addr = ('127.0.0.1', 49500)

        # The initial configuration of the robot at RSI startup
        self._joint_position_start = None

        # The actual and commanded joint positions with respective locks
        # Note that these are in degrees and not radians
        self._joint_position_command = None
        self._joint_position_command_lock = Lock()
        self._joint_position_actual = None
        self._joint_position_actual_lock = Lock()
        self._client_ipoc = None
        
        # A daemon thread that handles the communication with the external control client
        self._command_thread = Thread(target=self._run_command)
        self._command_thread.daemon = True
        self._command_thread.start()

    def _run_command(self):
        '''Function that handles communication with the control client.

        Note that disconnect and reconnect is not implemented.
        '''
        while True:
            # Recieve command position from control client in radians and
            # convert to degrees
            cmd_bytes, addr = self._cmd_server.recvfrom(1024)
            cmd_data = struct.unpack('Q6d', cmd_bytes)
            client_ipoc = cmd_data[0]
            cmd_joints_deg = np.rad2deg(cmd_data[1:])
            #print('Commaded joints: ', cmd_joints)
            with self._joint_position_command_lock:
                self._joint_position_command = cmd_joints_deg
                self._client_ipoc = client_ipoc
            # Yield to server thread
            time.sleep(0.001)
            # # It might be the case that the robot has not yet connected
            # if self._joint_position_actual is not None:
            #     # Send actual position in radians to control client
            #     with self._joint_position_actual_lock:
            #         self._cmd_server.send(struct.pack(
            #             '6d', *np.deg2rad(self._joint_position_actual)))

    def run(self):
        '''Function that handles the communication with robot controller'''
        glitch_count = 0
        # Receive first packet from the robot
        print('Awaiting RSI client connection')
        rsi_data = self._rsi_server.recv()
        print('RSI client connection obtained')
        # Parse and store the start configuration of the robot.
        # We need to send corrections to this configuration in each cycle
        self._joint_position_start, ipoc = self.parse_rsi_xml_start(rsi_data)
        print('Start position ({}): {}'.format(ipoc, self._joint_position_start))
        # Send a command with zero correction to the robot
        cmd = self.create_rsi_xml_cmd([0.0, 0.0, 0.0, 0.0, 0.0, 0.0], ipoc)
        self._rsi_server.send(cmd)
        print('Initial RSI command:', cmd)
        # The main loop of the server
        while True:
            # Receive and parse xml from robot controller
            rsi_data = self._rsi_server.recv()
            with self._joint_position_actual_lock:
                self._actual_flange, self._joint_position_actual, ipoc = self.parse_rsi_xml_act(
                    rsi_data)
                # Tell the client
                self._cmd_server.sendto(struct.pack(
                    'Q6d', ipoc,
                    *np.deg2rad(self._joint_position_actual - self._joint_zero_corr_deg)),
                                        self._cmd_addr)
            # print('IPOC: {}'.format(ipoc))
            # print('Actual joints: {}'.format(self._joint_position_actual))
            if self._rist:
                print('Actual flange: {}'.format(self._actual_flange))
            # Yield to command thread
            time.sleep(0.008)
            # It might be the case that the external control client has not yet connected
            if self._joint_position_command is not None:
                # Compute joint correction based on initial position and command position
                with self._joint_position_command_lock:
                    joint_position_correction = (self._joint_position_command
                                                 + self._joint_zero_corr_deg
                                                 - self._joint_position_start)
                    client_ipoc = self._client_ipoc
            else:
                # The external control client has not yet connected. Sending zero corrections.
                joint_position_correction = np.zeros(6)
                client_ipoc = 0
            # Form command xml and send to robot controller
            cmd = self.create_rsi_xml_cmd(joint_position_correction, ipoc)
            self._rsi_server.send(cmd)
            print('Commanded position :', self._joint_position_command)
            print('Client IPOC delta: ', ipoc - client_ipoc)
            print()
            #if ipoc - client_ipoc > 0:
            #    glitch_count += 1
            #    print('Glitch count: {}'.format(glitch_count))
            #else:
            #    print('No glitch')

    def create_rsi_xml_cmd(self, cmd_joint_corr, ipoc):
        q = cmd_joint_corr
        root = ET.Element('Sen', {'Type': 'ImFree'})
        ET.SubElement(root, 'AK',
                      dict(zip(['A'+str(i+1) for i in range(6)],
                               ['{:.5f}'.format(qi) for qi in q])))
        ET.SubElement(root, 'IPOC').text = str(ipoc)
        return ET.tostring(root)

    def parse_rsi_xml_start(self, data):
        root = ET.fromstring(data)
        ASPos = root.find('ASPos').attrib
        start_joint_position = np.array(
            [ASPos['A1'],
             ASPos['A2'],
             ASPos['A3'],
             ASPos['A4'],
             ASPos['A5'],
             ASPos['A6']],
            dtype=np.float64)
        ipoc = int(root.find('IPOC').text)
        return start_joint_position, ipoc

    def parse_rsi_xml_act(self, data):
        global root
        root = ET.fromstring(data)
        AIPos = root.find('AIPos').attrib
        actual_joint_position = np.array(
            [AIPos['A1'],
             AIPos['A2'],
             AIPos['A3'],
             AIPos['A4'],
             AIPos['A5'],
             AIPos['A6']],
            dtype=np.float64)
        ipoc = int(root.find('IPOC').text)
        actual_flange = None
        if self._rist:
            RIst = root.find('RIst').attrib
            actual_flange = np.array(
                [RIst['X'], RIst['Y'], RIst['Z'],
                 RIst['A'], RIst['B'], RIst['C']],
                dtype=np.float64)
        return actual_flange, actual_joint_position, ipoc



if __name__ == '__main__':
    import argparse
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--rsi_host', type=str, default='127.0.0.1')
    arg_parser.add_argument('--rist', action='store_true', default=False,
                            help="""Print the tool pose as reported in every cycle.""")
    args = arg_parser.parse_args()
    rsi_server = RsiServer(rsi_host=args.rsi_host, rist=args.rist)
    rsi_server.run()
