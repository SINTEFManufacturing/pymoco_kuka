# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "AGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import pymoco.robots
import pymoco.facades

# import robot definitions
from pymoco.robots.kuka_kr16_2 import KUKAKR16_2
from pymoco.robots.kuka_kr16_r2010_2 import KUKAKR16_R2010_2
from pymoco.robots.kuka_kr5arc import KUKAKR5Arc
from pymoco.robots.kuka_kr120r2500 import KUKAKR120R2500
from pymoco.robots.kuka_kr60_l30_3 import KUKAKR60_L30_3

# Import facades
from pymoco.facades.kuka_rsi_adapter import KUKARSIAdapterFacade
from pymoco.facades.kuka_kvp import KUKAKVPFacade

# Register robot definitions
for rob_cls in [KUKAKR16_R2010_2, KUKAKR16_2, KUKAKR5Arc, KUKAKR120R2500, KUKAKR60_L30_3]:
    pymoco.robots.register_robot_type(rob_cls)

# Register facades
pymoco.facades.register_facade_type(KUKARSIAdapterFacade)
pymoco.facades.register_facade_type(KUKAKVPFacade)
